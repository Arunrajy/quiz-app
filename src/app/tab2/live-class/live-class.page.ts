import { Component , ViewChild} from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-live-class',
  templateUrl: 'live-class.page.html',
  styleUrls: ['live-class.page.scss']
})
export class LiveClassPage {
  segmentModel:string="live class";
  slideOpts = {
    slidesPerView: 4,
    initialSlide: 0,
    spaceBetween: 10,
    pagination: false,
    speed: 400
  };
  @ViewChild('slides',{static:false}) slides:IonSlides
  constructor(private router:Router) {}
  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }
  prevSlide(){
    this.slides.slidePrev();
  }
  nextSlide(){
    this.slides.slideNext();
  }
  goBack(){
    this.router.navigate(['/outer-tabs']);
  }
}
