import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LiveClassPage } from './live-class/live-class.page';

const routes: Routes = [
  {
    path: '',
    component: LiveClassPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
