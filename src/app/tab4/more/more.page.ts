import { Component } from '@angular/core';
import { Router} from '@angular/router';
@Component({
  selector: 'app-more',
  templateUrl: 'more.page.html',
  styleUrls: ['more.page.scss']
})
export class MorePage {

  constructor(private router:Router) {}
  
  navigateTo(routeName){
    this.router.navigate(['/outer-tabs/tabs/tab4/'+ routeName])    
  }

  goBack(){
    this.router.navigate(['/outer-tabs']);
  }
}
