import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MorePage } from './more/more.page';
import { ResourcesPage} from './resources/resources.page';
import { CertificatePage } from './certificate/certificate.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab4PageRoutingModule } from './tab4-routing.module'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: MorePage }]),
    Tab4PageRoutingModule,
  ],
  declarations: [MorePage,ResourcesPage,CertificatePage]
})
export class Tab4PageModule {}
