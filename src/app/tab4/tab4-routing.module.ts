import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MorePage } from './more/more.page';
import { ResourcesPage} from './resources/resources.page';
import { CertificatePage } from './certificate/certificate.page';

const routes: Routes = [
  {
    path: '',
    component: MorePage,
  },
  {
    path: 'resources',
    component: ResourcesPage,
  },
  {
    path: 'certificate',
    component: CertificatePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab4PageRoutingModule {}
