import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AssessmentPage } from './assessment/assessment.page';
import { QuizPage } from './quiz/quiz.page';
import { SummaryPage } from './summary/summary.page';
import { PausePage } from './pause/pause.page';

const routes: Routes = [
  {
    path: '',
    component: AssessmentPage,
  },
  {
    path: 'quiz',
    component: QuizPage,
  },
  {
    path: 'summary',
    component: SummaryPage,
  },
  {
    path: 'pause',
    component: PausePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}
