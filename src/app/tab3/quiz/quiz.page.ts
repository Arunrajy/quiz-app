import { Component, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';

declare var require:any;
const data: any = require('./quiz.json');
@Component({
  selector: 'app-quiz',
  templateUrl: 'quiz.page.html',
  styleUrls: ['quiz.page.scss']
})
export class QuizPage {
  
  quizData=data;
  slideOpts = {
    slidesPerView: 1,
    initialSlide: 0,
    spaceBetween: 10,
    pagination: {
      el: '.swiper-pagination-header',
      type: 'fraction',
    },
    speed: 400
  };
  @ViewChild('slides',{static:false}) slides:IonSlides
  constructor(private router: Router) {}
  ionViewDidEnter(){
    this.slides.lockSwipes(true);
  }
  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }
  prevSlide(){
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }
  nextSlide(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }
  pauseQuiz(){
    this.router.navigate(['/outer-tabs/tabs/tab3/pause']);
  }
  finalSummary(){
    this.router.navigate(['/outer-tabs/tabs/tab3/summary']);

  }
}
