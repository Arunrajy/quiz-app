import { Component } from '@angular/core';
import { Router } from '@angular/router';
declare var require:any;
const data: any = require('./assessment.json');
@Component({
  selector: 'app-assessment',
  templateUrl: 'assessment.page.html',
  styleUrls: ['assessment.page.scss']
})
export class AssessmentPage {
  
  assessmentData=data;
  constructor(private router : Router) {}
  navigateTo(){
    this.router.navigate(['/outer-tabs/tabs/tab3/quiz']);
  }
  goBack(){
    this.router.navigate(['/outer-tabs']);
  }
}
