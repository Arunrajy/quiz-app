import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AssessmentPage } from './assessment/assessment.page';
import { QuizPage } from './quiz/quiz.page';
import { SummaryPage } from './summary/summary.page';
import { PausePage } from './pause/pause.page';

import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab3PageRoutingModule } from './tab3-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: AssessmentPage }]),
    Tab3PageRoutingModule,
  ],
  declarations: [AssessmentPage,QuizPage,SummaryPage,PausePage]
})
export class Tab3PageModule {}
