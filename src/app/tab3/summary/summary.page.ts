import { Component } from '@angular/core';

declare var require:any;
const data: any = require('./summary.json');
@Component({
  selector: 'app-summary',
  templateUrl: 'summary.page.html',
  styleUrls: ['summary.page.scss']
})
export class SummaryPage {
  
  summaryData=data;

  constructor() {}
  ionViewDidEnter(){
  }

}
