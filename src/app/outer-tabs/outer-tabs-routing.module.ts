import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OuterTabsPage } from './outer-tabs.page';
import { TabsPage } from "./tabs/tabs.page";
const routes: Routes = [
  {
    path: 'outer-tabs',
    component: OuterTabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'my-courses',
        loadChildren: () => import('../my-courses/my-courses.module').then(m => m.MyCoursesPageModule)
      },
      {
        path: 'account',
        loadChildren: () => import('../account/account.module').then(m => m.AccountPageModule)
      },
      {
        path: 'tabs',
        component: TabsPage,
        children: [
          {
            path: 'tab1',
            loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          },
          {
            path: 'tab2',
            loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          },
          {
            path: 'tab3',
            loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          },
          {
            path: 'tab4',
            loadChildren: () => import('../tab4/tab4.module').then(m => m.Tab4PageModule)
          },
          {
            path: '',
            redirectTo: '/outer-tabs/tabs/tab1',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/outer-tabs/home',
        pathMatch: 'full'
      }
    ]
  },

  {
    path: '',
    redirectTo: '/outer-tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OuterTabsPageRoutingModule {}
