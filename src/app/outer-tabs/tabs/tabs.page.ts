import { Component } from '@angular/core';
import { Router,NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  hideTab:boolean;
  tabPath = "outer-tabs/tabs/";
  constructor(private router:Router) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
      let url =val.url;
      let parts = url.split('/');
      console.log(parts)
      if((parts.indexOf("quiz") > -1) || (parts.indexOf("pause") > -1) || (parts.indexOf("summary") > -1)){
        this.hideTab = true;
      }else{
        this.hideTab = false;
      }
    }
    })
  }
  clickTab(event: Event, tab: string) {
    event.stopImmediatePropagation();
    console.log( event, tab );
    this.router.navigate([`${this.tabPath}${tab}`]);
  }
}
