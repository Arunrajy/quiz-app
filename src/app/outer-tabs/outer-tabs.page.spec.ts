import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OuterTabsPage } from './outer-tabs.page';

describe('OuterTabsPage', () => {
  let component: OuterTabsPage;
  let fixture: ComponentFixture<OuterTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OuterTabsPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OuterTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
