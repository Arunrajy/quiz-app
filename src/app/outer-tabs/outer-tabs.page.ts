import { Component } from '@angular/core';
import { Router,NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-outer-tabs',
  templateUrl: 'outer-tabs.page.html',
  styleUrls: ['outer-tabs.page.scss']
})
export class OuterTabsPage {
  hideTab:boolean;
  tabPath = "outer-tabs/";
  tabPath2= "/tab1";
  constructor(private router:Router) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
      let url =val.url;
      let parts = url.split('/');
      console.log(parts)
      if((parts.indexOf("notifications") > -1) || (parts.indexOf("tabs") > -1)){
        this.hideTab = true;
      }else{
        this.hideTab = false;
      }
    }
    })
  }

}
