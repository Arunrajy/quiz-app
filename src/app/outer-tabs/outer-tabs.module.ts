import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { OuterTabsPageRoutingModule } from './outer-tabs-routing.module';

import { OuterTabsPage } from './outer-tabs.page';
import { TabsPage } from './tabs/tabs.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    OuterTabsPageRoutingModule
  ],
  declarations: [OuterTabsPage,TabsPage]
})
export class OuterTabsPageModule {}
