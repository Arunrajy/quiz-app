import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ELearningPage } from './e-learning/e-learning.page';

const routes: Routes = [
  {
    path: '',
    component: ELearningPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
