import { Component,ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

declare var require:any;
const data: any = require('./e-learning.json');
@Component({
  selector: 'app-e-learning',
  templateUrl: 'e-learning.page.html',
  styleUrls: ['e-learning.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ELearningPage {
  videoSrc:any;
  learningData= data;
  videoElement:any;
  constructor(private router:Router) {}
  ionViewDidEnter(){
    this.videoSrc = this.learningData[0].videoUrl;
  }
  playVideo(videoData,videoElement){
    this.videoElement = videoElement;
    this.videoElement.playsInline = true;
    this.videoElement.src = videoData;
    if(this.videoElement.paused){
      this.videoElement.play();
    }else{
      this.videoElement.pause();
    }


  }
  ionViewDidLeave(){
    if(this.videoElement){
    this.videoElement.pause();
    }
  }
  goBack(){
    this.router.navigate(['/outer-tabs']);
  }
//   capture(){
//     let canvas = document.getElementById('canvas');
//     let video = document.getElementById('video');
//     canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
// }
}
