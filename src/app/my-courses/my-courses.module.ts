import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyCoursesPage } from './my-courses/my-courses.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { MyCoursesPageRoutingModule } from './my-courses-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    MyCoursesPageRoutingModule
  ],
  declarations: [MyCoursesPage]
})
export class MyCoursesPageModule {}
