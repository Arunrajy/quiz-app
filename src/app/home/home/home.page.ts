import { Component, ViewEncapsulation } from '@angular/core';
declare var require:any;
const data: any = require('./home.json');
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomePage {
  homeData= data;
  topCourses:any;
  constructor() {
    
  }
  ionViewDidEnter(){
    this.topCourses = this.homeData.topCourses;
  }

}
