import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountPage } from './account/account.page';
import { NotificationsPage } from './notifications/notifications.page';

const routes: Routes = [
  {
    path: '',
    component: AccountPage,
  },
  {
    path: 'notifications',
    component: NotificationsPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPageRoutingModule {}
